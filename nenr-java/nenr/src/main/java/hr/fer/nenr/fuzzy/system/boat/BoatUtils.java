package hr.fer.nenr.fuzzy.system.boat;

import hr.fer.nenr.fuzzy.domain.Domain;
import hr.fer.nenr.fuzzy.domain.IDomain;
import hr.fer.nenr.fuzzy.set.CalculatedFuzzySet;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.utils.IIntUnaryFunction;

public class BoatUtils {

    public static IIntUnaryFunction IDENTITY_FUNCTION = x -> 1;

    public static IDomain accelerationDomain() {
        return Domain.intRange(-100, 101);
    }

    public static IDomain angleDomain() {
        return Domain.intRange(-90, 91);
    }

    public static IDomain distanceDomain() {
        return Domain.intRange(0, 1301);
    }

    public static IDomain velocityDomain() {
        return Domain.intRange(0, 101);
    }

    public static IDomain directionDomain() {
        return Domain.intRange(0, 2);
    }

    public static IFuzzySet distanceSet() {
        return new CalculatedFuzzySet(distanceDomain(), IDENTITY_FUNCTION);
    }

    public static IFuzzySet angleSet() {
        return new CalculatedFuzzySet(angleDomain(), IDENTITY_FUNCTION);
    }

    public static IFuzzySet velocitySet() {
        return new CalculatedFuzzySet(velocityDomain(), IDENTITY_FUNCTION);
    }

    public static IFuzzySet directionSet() {
        return new CalculatedFuzzySet(directionDomain(), IDENTITY_FUNCTION);
    }

    public static class Input {

        private int left;
        private int right;
        private int rightAngle;
        private int leftAngle;
        private int speed;
        private int direction;

        public Input(int left, int right, int leftAngle, int rightAngle,
                     int speed,
                     int direction) {
            this.left = left;
            this.right = right;
            this.rightAngle = rightAngle;
            this.leftAngle = leftAngle;
            this.speed = speed;
            this.direction = direction;
        }

        public int getLeft() {
            return left;
        }

        public int getRight() {
            return right;
        }

        public int getRightAngle() {
            return rightAngle;
        }

        public int getLeftAngle() {
            return leftAngle;
        }

        public int getSpeed() {
            return speed;
        }

        public int getDirection() {
            return direction;
        }

        public int[] toArray() {
            return new int[]{left, right, leftAngle, rightAngle, speed,
                    direction};
        }
    }
}
