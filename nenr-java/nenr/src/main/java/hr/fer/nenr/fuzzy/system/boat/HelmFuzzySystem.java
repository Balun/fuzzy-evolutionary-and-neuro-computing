package hr.fer.nenr.fuzzy.system.boat;

import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.operations.Operations;
import hr.fer.nenr.fuzzy.set.CalculatedFuzzySet;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.set.MutableFuzzySet;
import hr.fer.nenr.fuzzy.system.Defuzzifier;
import hr.fer.nenr.fuzzy.system.FuzzySystem;
import hr.fer.nenr.fuzzy.system.rule.Base;
import hr.fer.nenr.fuzzy.utils.IBinaryFunction;

import java.util.List;

import static hr.fer.nenr.fuzzy.system.boat.BoatUtils.*;
import static hr.fer.nenr.fuzzy.set.StandardFuzzySets.*;

public class HelmFuzzySystem extends FuzzySystem {

    public static final IBinaryFunction S_NORM = Operations.zadehOr();

    public static final IBinaryFunction T_NORM = Operations.zadehAnd();

    public static final IBinaryFunction IMPLICATION = Operations.zadehAnd();

    private static final IFuzzySet STEER_RIGHT =
            new CalculatedFuzzySet(angleDomain(),
                    lFunction(angleDomain().indexOfElement(DomainElement.of(-40)),
                            angleDomain().indexOfElement(DomainElement.of(-10))));

    private static final IFuzzySet STEER_HARD_RIGHT =
            new CalculatedFuzzySet(angleDomain(),
                    lFunction(angleDomain().indexOfElement(DomainElement.of(-75)),
                            angleDomain().indexOfElement(DomainElement.of(-30))));

    private static final IFuzzySet STEER_LEFT =
            new CalculatedFuzzySet(angleDomain(),
                    gammaFunction(angleDomain().indexOfElement(DomainElement.of(10)),
                            angleDomain().indexOfElement(DomainElement.of(40))));

    private static final IFuzzySet STEER_HARD_LEFT =
            new CalculatedFuzzySet(angleDomain(),
                    gammaFunction(angleDomain().indexOfElement(DomainElement.of(30)),
                            angleDomain().indexOfElement(DomainElement.of(75))));

    private static final IFuzzySet WALL_CLOSE =
            new CalculatedFuzzySet(distanceDomain(),
                    lFunction(distanceDomain().indexOfElement(DomainElement.of(45)),
                            distanceDomain().indexOfElement(DomainElement.of(85))));

    private static final IFuzzySet WALL_CLOSE_WITH_ANGLE =
            new CalculatedFuzzySet(distanceDomain(),
                    lFunction(distanceDomain().indexOfElement(DomainElement.of(60)),
                            distanceDomain().indexOfElement(DomainElement.of(180))));

    private static final IFuzzySet INCORRCT_ORIENTATION =
            new MutableFuzzySet(directionDomain()).set(DomainElement.of(0), 1);

    public static final Base HELM_RULE_BASE;

    static {
        HELM_RULE_BASE = new Base(S_NORM, T_NORM, IMPLICATION);


        HELM_RULE_BASE.add(List.of(distanceSet(), distanceSet(), distanceSet(), WALL_CLOSE_WITH_ANGLE, velocitySet(), directionSet()), STEER_LEFT);

        HELM_RULE_BASE.add(List.of(distanceSet(), distanceSet(),
                WALL_CLOSE_WITH_ANGLE, distanceSet(), velocitySet(),
                directionSet()), STEER_RIGHT);

        HELM_RULE_BASE.add(List.of(distanceSet(), WALL_CLOSE, distanceSet(),
                distanceSet(), velocitySet(), directionSet()), STEER_HARD_LEFT);

        HELM_RULE_BASE.add(List.of(WALL_CLOSE, distanceSet(), distanceSet(), distanceSet(), velocitySet(), directionSet()), STEER_HARD_RIGHT);

        HELM_RULE_BASE.add(List.of(distanceSet(), distanceSet(),
                distanceSet(), distanceSet(), velocitySet(), INCORRCT_ORIENTATION),
                STEER_HARD_LEFT);
    }

    public HelmFuzzySystem(Defuzzifier def) {
        super(HELM_RULE_BASE, def);
    }
}
