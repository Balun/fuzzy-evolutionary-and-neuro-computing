package hr.fer.nenr.fuzzy.utils;

public interface IUnaryFunction {

    double valueAt(double value);
}
