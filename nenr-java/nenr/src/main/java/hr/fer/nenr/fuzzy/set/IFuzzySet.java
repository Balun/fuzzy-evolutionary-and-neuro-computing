package hr.fer.nenr.fuzzy.set;

import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.domain.IDomain;

public interface IFuzzySet {

    IDomain getDomain();

    double getValueAt(DomainElement element);
}
