package hr.fer.nenr.fuzzy.system.rule;

import hr.fer.nenr.fuzzy.operations.Operations;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.utils.IBinaryFunction;

import java.util.ArrayList;
import java.util.List;

public class Base {

    private List<Rule> rules;


    private IBinaryFunction sNorm;

    private IBinaryFunction tNorm;

    private IBinaryFunction implication;

    public Base(IBinaryFunction sNorm, IBinaryFunction tNorm,
                IBinaryFunction implication) {
        this.rules = new ArrayList<>();
        this.sNorm = sNorm;
        this.tNorm = tNorm;
        this.implication = implication;
    }

    public Base add(List<IFuzzySet> antecedent, IFuzzySet consequent) {
        this.rules.add(new Rule(antecedent, consequent, tNorm, implication));
        return this;
    }

    public Rule get(int i) {
        return this.rules.get(i);
    }

    public IFuzzySet conclude(int... vars) {
        IFuzzySet conclusion = rules.get(0).conclude(vars);

        for (int i = 1, end = rules.size(); i < end; i++) {
            conclusion =
                    Operations.binaryOperation(conclusion,
                            rules.get(i).conclude(vars),
                            sNorm);
        }
        return conclusion;
    }
}
