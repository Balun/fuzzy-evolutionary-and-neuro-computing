package hr.fer.nenr.fuzzy.system.rule;

import hr.fer.nenr.fuzzy.domain.Domain;
import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.domain.IDomain;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.set.MutableFuzzySet;
import hr.fer.nenr.fuzzy.utils.IBinaryFunction;

import java.util.List;

public class Rule {

    private List<IFuzzySet> antecedent;

    private IFuzzySet consequent;

    private IBinaryFunction tNorm;

    private IBinaryFunction implication;

    public Rule(List<IFuzzySet> antecedent, IFuzzySet consequent, IBinaryFunction tNorm,
                IBinaryFunction implication) {
        this.antecedent = antecedent;
        this.consequent = consequent;
        this.tNorm = tNorm;
        this.implication = implication;
    }

    public IFuzzySet conclude(int... vars) {
        IDomain domain = consequent.getDomain();
        MutableFuzzySet conclusion = new MutableFuzzySet(domain);

        for (DomainElement element : domain) {
            double value = antecedent.get(0).getValueAt(DomainElement.of(vars[0]));

            for (int i = 1, end = antecedent.size(); i < end; i++) {
                value = tNorm.valueAt(value,
                    antecedent.get(i).getValueAt(DomainElement.of(vars[i])));
            }

            conclusion.set(element, implication.valueAt(value,
                    consequent.getValueAt(element)));
        }

        return conclusion;
    }
}
