package hr.fer.nenr.fuzzy.system;

import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.set.IFuzzySet;

public class COADefuzzifier implements Defuzzifier {


    @Override
    public int defuzzify(IFuzzySet set) {
        double sumOfValues = 0;
        double sumOfValuesAndElaments = 0;

        for (DomainElement element : set.getDomain()) {
            sumOfValuesAndElaments += set.getValueAt(element) * element.getComponentValue(0);
            sumOfValues += set.getValueAt(element);
        }

        return (int) Math.round(sumOfValuesAndElaments / sumOfValues);
    }
}
