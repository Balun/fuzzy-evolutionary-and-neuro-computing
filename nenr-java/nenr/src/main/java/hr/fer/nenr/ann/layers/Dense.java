package hr.fer.nenr.ann.layers;

import hr.fer.nenr.ann.neurons.Neuron;
import org.apache.commons.collections4.iterators.ObjectArrayIterator;

import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Function;

public class Dense implements ILayer {

    protected Neuron[] neurons;

    public Dense(int size, int nextLayerSize,
                 Function<Double, Double> activation) {
        neurons = new Neuron[size];

        for (int i = 0; i < size; i++) {
            neurons[i] = new Neuron(i, nextLayerSize, activation);
        }
    }

    public Dense(int size, int nextLayerSize) {
        neurons = new Neuron[size];

        for (int i = 0; i < size; i++) {
            neurons[i] = new Neuron(i, nextLayerSize);
        }
    }

    @Override
    public Neuron getNeuron(int i) {
        return neurons[i];
    }

    @Override
    public int size() {
        return neurons.length;
    }

    @Override
    public void solveLayer(ILayer prev) {
        forEach(n -> n.calculateY(prev));
    }

    @Override
    public void updateDeltas(ILayer next) {
        forEach(n -> n.updateDelta(next));
    }

    @Override
    public void updateWeigths(ILayer next, double learningRate) {
        forEach(n -> n.updateWeights(next, learningRate));
    }

    @Override
    public void swapWeights() {
        forEach(Neuron::swapWeights);
    }

    @Override
    public Iterator<Neuron> iterator() {
        return new ObjectArrayIterator<>(neurons);
    }

    @Override
    public void forEach(Consumer<? super Neuron> action) {
        for (Neuron e : neurons) {
            action.accept(e);
        }
    }
}
