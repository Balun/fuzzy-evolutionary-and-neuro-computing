package hr.fer.nenr.fuzzy.utils;

public interface IBinaryFunction {

    double valueAt(double first, double second);
}
