package hr.fer.nenr.ann.layers;

import hr.fer.nenr.ann.neurons.Neuron;

public interface ILayer extends Iterable<Neuron> {

    Neuron getNeuron(int i);

    int size();

    void solveLayer(ILayer prev);

    void updateDeltas(ILayer next);

    void updateWeigths(ILayer next, double learningRate);

    void swapWeights();
}
