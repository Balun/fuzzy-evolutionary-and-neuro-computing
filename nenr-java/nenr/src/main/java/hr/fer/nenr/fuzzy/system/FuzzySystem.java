package hr.fer.nenr.fuzzy.system;

import hr.fer.nenr.fuzzy.system.rule.Base;

public abstract class FuzzySystem {

    private Base HELM_RULE_BASE;

    private Defuzzifier def;

    public FuzzySystem(Base ruleBase, Defuzzifier def) {
        this.HELM_RULE_BASE = ruleBase;
        this.def = def;
    }

    public int conclude(int... variables) {
        return def.defuzzify(HELM_RULE_BASE.conclude(variables));
    }
}
