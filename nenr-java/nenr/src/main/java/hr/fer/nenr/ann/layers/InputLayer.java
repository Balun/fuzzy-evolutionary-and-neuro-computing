package hr.fer.nenr.ann.layers;

import hr.fer.nenr.ann.neurons.InputNeuron;
import hr.fer.nenr.ann.neurons.Neuron;

public class InputLayer extends Dense {
    public InputLayer(int size, int nextLayerSize) {
        super(size, nextLayerSize);

        for (int i = 0; i < size(); i++) {
            neurons[i] = new InputNeuron(neurons[i].getId(), nextLayerSize);
        }
    }
}
