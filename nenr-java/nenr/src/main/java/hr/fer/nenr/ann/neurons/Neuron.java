package hr.fer.nenr.ann.neurons;

import hr.fer.nenr.ann.Utils;
import hr.fer.nenr.ann.layers.ILayer;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Objects;
import java.util.Random;
import java.util.function.Function;

public class Neuron {

    private static Random RAND = new Random();

    private int id;

    private double y;

    private double w0;

    private double delta;

    private double[] weights;

    private double[] tmpWeights;

    private Function<Double, Double> activation;

    public Neuron(int id,
                  int nextLayerSize,
                  Function<Double, Double> activation) {

        this.id = id;
        this.activation = activation;
        this.delta = 0;

        initialiseWeights(nextLayerSize);

        this.y = w0;
    }

    public Neuron(int id, int nexLayerSize) {
        this(id, nexLayerSize, Utils.SIGMOID);
    }

    public int getId() {
        return id;
    }

    public double getY() {
        return y;
    }

    public double calculateY(ILayer prev) {
        double net = 0;

        for (Neuron n : prev) {
            net += n.getY() * n.weights[id];
        }

        this.y = activation.apply(net);
        return this.y;
    }

    public double getW0() {
        return w0;
    }

    public double getDelta() {
        return delta;
    }

    public void setW0(double w0) {
        this.w0 = w0;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    public void updateDelta(ILayer next) {
        this.delta = 0;

        for (int i = 0; i < next.size(); i++) {
            this.delta += getY() * (1 - getY()) * weights[i] * next.getNeuron(i).getDelta();
        }
    }

    public void updateWeights(ILayer next, double learningRate) {
        for (int i = 0; i < next.size(); i++) {
            tmpWeights[i] = tmpWeights[i] + learningRate * getY() * next.getNeuron(i).getDelta();
        }
    }

    public void swapWeights() {
        weights = ArrayUtils.clone(tmpWeights);
    }

    private void initialiseWeights(int size) {
        this.w0 = RAND.nextDouble() * 4 - 2.0;

        this.weights = new double[size];

        for (int i = 0; i < size; i++) {
            weights[i] = RAND.nextDouble() * 4 - 2.0;
        }

        this.tmpWeights = ArrayUtils.clone(weights);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Neuron neuron = (Neuron) o;
        return id == neuron.id &&
                Double.compare(neuron.y, y) == 0 &&
                Double.compare(neuron.w0, w0) == 0 &&
                Double.compare(neuron.delta, delta) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, y, w0, delta);
    }
}
