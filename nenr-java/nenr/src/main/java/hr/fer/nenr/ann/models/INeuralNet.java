package hr.fer.nenr.ann.models;

import hr.fer.nenr.ann.layers.ILayer;

public interface INeuralNet extends Iterable<ILayer> {

    INeuralNet add(ILayer layer);

    INeuralNet fit(double[][] samples, double[][] labels,
                   double learningRate, int epochs, int batchSize);

    INeuralNet fit(String dataPath,
                   double learningRate, int epochs, int batchSize);

    double getError(double[][] samples, double[][] labels);

    double[] predict(double[] sample);

    double[][] predict(double[][] samples);

    int predictLabel(double[] sample);

    int[] predictLabels(double[][] samples);
}
