package hr.fer.nenr.ui.ann.maker;

import javax.swing.table.AbstractTableModel;
import java.util.*;

public class ClassificationMakerModel<V> extends AbstractTableModel {

    private Map<Integer, List<V>> data;

    private List<DataListener> listeners;

    private int currentClass;

    public ClassificationMakerModel(Map<Integer, List<V>> data) {
        this.data = data;
        this.listeners = new ArrayList<>();
        this.currentClass = 0;
    }

    public ClassificationMakerModel() {
        this(new HashMap<>());
    }

    public void add(int key, V sample) {
        if (!data.containsKey(key)) {
            data.put(key, new ArrayList<>());
        }

        data.get(key).add(sample);
        fire();
    }

    public void add(V sample) {
        add(currentClass, sample);
    }

    public Collection<V> get(int key) {
        return data.get(key);
    }

    public V get(int key, int index) {
        return data.get(key).get(index);
    }

    public Map<Integer, List<V>> getData() {
        return data;
    }

    public Map<Integer, Integer> getClassCount() {
        Map<Integer, Integer> countMap = new HashMap<>();

        for (int key : data.keySet()) {
            countMap.put(key, data.get(key).size());
        }

        return countMap;
    }

    public void clearData(){
        data.clear();
        currentClass = 0;
        fire();
    }

    public int getCurrentClass() {
        return currentClass;
    }

    public void setCurrentClass(int currentClass) {
        this.currentClass = currentClass;
    }

    public void incrementClass() {
        currentClass++;
    }

    public void addDataListener(DataListener listener) {
        listeners.add(listener);
    }

    public void removeDataListener(DataListener listener) {
        listeners.remove(listener);
    }

    private void fire() {
        listeners.forEach(DataListener::dataChanged);
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        return i1 == 0 ? i : data.get(i).size();
    }

    @Override
    public String getColumnName(int column) {
        return column == 0 ? "class" : "count";
    }
}
