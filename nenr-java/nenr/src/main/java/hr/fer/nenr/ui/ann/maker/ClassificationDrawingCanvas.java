package hr.fer.nenr.ui.ann.maker;

import hr.fer.nenr.ui.ann.AbstractDrawingCanvas;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class ClassificationDrawingCanvas extends AbstractDrawingCanvas {

    private ClassificationMakerModel<List<Point2D.Double>> model;

    public ClassificationDrawingCanvas(ClassificationMakerModel<List<Point2D.Double>>
                                               model) {
        super();
        this.model = model;
    }

    public ClassificationMakerModel<List<Point2D.Double>> getModel() {
        return model;
    }

    @Override
    protected void canvasMouseReleased(MouseEvent evt) {
        points.add(new Point2D.Double(evt.getX(), evt.getY()));
        this.repaint();
        model.add(new ArrayList<>(points));
    }
}
