package hr.fer.nenr.demo.lab3;

import hr.fer.nenr.fuzzy.system.COADefuzzifier;
import hr.fer.nenr.fuzzy.system.Defuzzifier;
import hr.fer.nenr.fuzzy.system.FuzzySystem;
import hr.fer.nenr.fuzzy.system.boat.AcceleratorFuzzySystem;
import hr.fer.nenr.fuzzy.system.boat.BoatUtils;
import hr.fer.nenr.fuzzy.system.boat.HelmFuzzySystem;

import java.util.Scanner;

public class Boat {

    public static void main(String[] args) {
        Defuzzifier def = new COADefuzzifier();

        FuzzySystem helmSystem = new HelmFuzzySystem(def);
        FuzzySystem acceleratorSystem = new AcceleratorFuzzySystem(def);

        try (Scanner sc = new Scanner(System.in)) {
            while (sc.hasNextLine()) {
                String line = sc.nextLine();

                if (line.equals("KRAJ")) {
                    break;
                }

                BoatUtils.Input input = parse(line);

                int acceleration = acceleratorSystem.conclude(input.toArray());
                int helm = helmSystem.conclude(input.toArray());

                System.err.println(acceleration + " " + helm);

                System.out.println(acceleration + " " + helm);
                System.out.flush();
            }
        }
    }

    private static BoatUtils.Input parse(String line) {
        String[] chunks = line.split(" ");

        return new BoatUtils.Input(Integer.parseInt(chunks[0]),
                Integer.parseInt(chunks[1]), Integer.parseInt(chunks[2]),
                Integer.parseInt(chunks[3]), Integer.parseInt(chunks[4]),
                Integer.parseInt(chunks[5]));
    }

}
