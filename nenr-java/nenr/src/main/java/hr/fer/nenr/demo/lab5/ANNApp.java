package hr.fer.nenr.demo.lab5;

import hr.fer.nenr.ui.ann.ANNMainMenu;

import javax.swing.SwingUtilities;

public class ANNApp {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new ANNMainMenu("Artificial neural " +
                "network demo").setVisible(true));
    }
}
