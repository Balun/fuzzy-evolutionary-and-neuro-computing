package hr.fer.nenr.ui.ann.train;

import hr.fer.nenr.ann.Utils;
import hr.fer.nenr.ui.ann.maker.DataListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

public class ValidationPanel extends JPanel {

    private TrainModel model;

    public ValidationPanel(TrainModel model) {
        this.model = model;
        setLayout(new BorderLayout());

        initGUI();
    }

    private void initGUI() {
        add(new ValidationDrawingCanvas(this.model), BorderLayout.CENTER);
        PercentagePanel perc = new PercentagePanel();
        model.addDataListener(perc);
        add(perc, BorderLayout.NORTH);
    }

    private class PercentagePanel extends JPanel implements DataListener {


        public PercentagePanel() {
            setBackground(new Color(73, 199, 221));
        }

        @Override
        public void dataChanged() {
            this.removeAll();

            double[] prediction = model.getCurrentPrediction();

            this.setLayout(new GridLayout(0, prediction.length));

            for (int i = 0; i < prediction.length; i++) {
                this.add(new JLabel(String.format("%s: %.3f%%",
                        Utils.getLetterByIndex(i),
                        prediction[i] * 100), SwingConstants.CENTER));
            }

            this.revalidate();
        }
    }
}
