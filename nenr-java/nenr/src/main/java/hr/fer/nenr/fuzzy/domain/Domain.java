package hr.fer.nenr.fuzzy.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public abstract class Domain implements IDomain {


    public static IDomain intRange(int begin, int end) {
        if (begin >= end) {
            throw new IllegalArgumentException("Error in constraints");
        }

        return new SimpleDomain(begin, end);
    }

    public static Domain combine(IDomain first, IDomain second) {
        List<SimpleDomain> domains = new LinkedList<>();

        if (first instanceof CompositeDomain) {
            for (int i = 0; i < first.getNumberOfComponents(); i++) {
                domains.add((SimpleDomain) first.getComponent(i));
            }
        } else {
            domains.add((SimpleDomain) first);
        }

        if (second instanceof CompositeDomain) {
            for (int i = 0; i < second.getNumberOfComponents(); i++) {
                domains.add((SimpleDomain) second.getComponent(i));
            }
        } else {
            domains.add((SimpleDomain) second);
        }

        return new CompositeDomain(domains.toArray(new SimpleDomain[0]));
    }

    public int indexOfElement(DomainElement element) {
        int i = 0;
        for (DomainElement el : this) {
            if (el.equals(element)) {
                return i;
            }
            i++;
        }

        throw new NoSuchElementException();
    }

    public DomainElement elementForIndex(int index) {
        int i = 0;

        for (DomainElement el : this) {
            if (i == index) {
                return el;
            }

            i++;
        }

        throw new IndexOutOfBoundsException();
    }

}
