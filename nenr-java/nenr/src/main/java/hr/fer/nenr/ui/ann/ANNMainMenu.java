package hr.fer.nenr.ui.ann;


import hr.fer.nenr.ui.ann.maker.ClassificationMakerModel;
import hr.fer.nenr.ui.ann.maker.ClassificationMakerPanel;
import hr.fer.nenr.ui.ann.train.TrainModel;
import hr.fer.nenr.ui.ann.train.TrainModelPanel;
import hr.fer.nenr.ui.ann.train.ValidationPanel;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.geom.Point2D;
import java.util.List;

public class ANNMainMenu extends JFrame {

    private ClassificationMakerModel<List<Point2D.Double>> model;

    public ANNMainMenu(String title) throws HeadlessException {
        super(title);

        this.setSize(700, 700);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        initGUI();
    }

    private void initGUI() {
        this.setLayout(new BorderLayout());
        this.add(createTabbedPane(), BorderLayout.CENTER);
    }

    private JComponent createTabbedPane() {
        JTabbedPane tabbed_pane = new JTabbedPane();

        ClassificationMakerModel<List<Point2D.Double>> makerModel = new ClassificationMakerModel<>();
        TrainModel trainModel = new TrainModel();

        tabbed_pane.add("Create dataset", new ClassificationMakerPanel(makerModel));
        tabbed_pane.add("Train model", new TrainModelPanel(trainModel));
        tabbed_pane.add("Validate", new ValidationPanel(trainModel));

        return tabbed_pane;
    }
}
