package hr.fer.nenr.fuzzy.domain;


import java.util.*;

public class CompositeDomain extends Domain {

    private List<IDomain> domains;

    public CompositeDomain(SimpleDomain... domains) {
        this.domains = Arrays.asList(domains);
    }

    @Override
    public int getCardinality() {
        int prod = 1;
        for (IDomain dom : domains) {
            prod *= dom.getCardinality();
        }

        return prod;
    }

    @Override
    public IDomain getComponent(int index) {
        return domains.get(index);
    }

    @Override
    public int getNumberOfComponents() {
        return domains.size();
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return new CompositeDomainIterator();
    }

    private class CompositeDomainIterator implements Iterator<DomainElement> {

        private boolean done;

        private int cnt;
        private int[] cursorValue;

        public CompositeDomainIterator() {
            done = false;
            cnt = CompositeDomain.this.getNumberOfComponents();
            cursorValue = new int[cnt];
            for (int i = 0; i < cnt; ++i) {
                cursorValue[i] = ((SimpleDomain) CompositeDomain.this.getComponent(i)).getFirst();
            }
        }

        private boolean finished() {
            for (int i = 0; i < cnt; ++i) {
                if (cursorValue[i] < ((SimpleDomain) CompositeDomain.this.getComponent(i)).getLast() - 1)
                    return false;
            }
            return true;
        }

        @Override
        public boolean hasNext() {
            return !this.done;
        }

        @Override
        public DomainElement next() {
            if (this.hasNext()) {
                int[] oldCursor = new int[cnt];
                oldCursor = Arrays.copyOf(cursorValue, cnt);
                DomainElement ret = DomainElement.of(oldCursor);
                this.done = finished();
                for (int i = cnt - 1; i >= 0; --i) {
                    if (cursorValue[i] == ((SimpleDomain) CompositeDomain.this.getComponent(i)).getLast() - 1)
                        continue;
                    ++cursorValue[i];
                    for (int j = i + 1; j < cnt; ++j)
                        cursorValue[j] = ((SimpleDomain) CompositeDomain.this.getComponent(j)).getFirst();
                    break;
                }
                return ret;
            }
            throw new NoSuchElementException();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeDomain that = (CompositeDomain) o;
        return Objects.equals(domains, that.domains);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domains);
    }
}