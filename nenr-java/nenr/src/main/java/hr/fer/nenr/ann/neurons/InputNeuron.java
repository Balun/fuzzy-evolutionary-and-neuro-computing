package hr.fer.nenr.ann.neurons;

import hr.fer.nenr.ann.layers.ILayer;

public class InputNeuron extends Neuron {

    public InputNeuron(int id, int nextLayerSize) {
        super(id, nextLayerSize);
    }

    @Override
    public double getY() {
        return getW0();
    }

    @Override
    public double calculateY(ILayer prev) {
        return getW0();
    }

    @Override
    public void updateWeights(ILayer next, double learningRate) {
        super.updateWeights(next, learningRate);
    }
}
