package hr.fer.nenr.ui.ann.maker;

public interface DataListener {

    void dataChanged();
}
