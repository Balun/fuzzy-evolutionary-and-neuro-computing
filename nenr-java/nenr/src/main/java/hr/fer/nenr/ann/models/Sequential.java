package hr.fer.nenr.ann.models;

import hr.fer.nenr.ann.Utils;
import hr.fer.nenr.ann.layers.Dense;
import hr.fer.nenr.ann.layers.ILayer;
import hr.fer.nenr.ann.layers.InputLayer;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.IntStream;

public class Sequential implements INeuralNet {

    private List<ILayer> layers;

    public Sequential() {
        this(new ArrayList<>());
    }

    public Sequential(List<ILayer> layers) {
        this.layers = layers;
    }

    public Sequential(ILayer... layers) {
        this(Arrays.asList(layers));
    }

    public Sequential(int... layers) {
        this(Utils.SIGMOID, layers);
    }

    public Sequential(Function<Double, Double> activation, int... layers) {
        this();

        for (int i = 0; i < layers.length; i++) {
            if (i == 0) {
                this.layers.add(new InputLayer(layers[i], layers[i + 1]));

            } else if (i == (layers.length - 1)) {
                this.layers.add(new Dense(layers[i], 0, activation));

            } else {
                this.layers.add(new Dense(layers[i], layers[i + 1],
                        activation));
            }
        }
    }

    @Override
    public INeuralNet add(ILayer layer) {
        layers.add(layer);
        return this;
    }

    @Override
    public INeuralNet fit(double[][] samples, double[][] labels,
                          double learningRate,
                          int epochs, int batchSize) {

        for (int i = 0; i < epochs; i++) {
            double error = getError(samples, labels);
            System.out.println("epoch -> " + i + ", error -> " + error);
            updateWeights(samples, labels, batchSize, learningRate);
        }

        return this;
    }

    @Override
    public INeuralNet fit(String dataPath, double learningRate, int epochs,
                          int batchSize) {

        Pair<double[][], double[][]> data = Utils.dataFromFile(dataPath);

        return fit(data.getLeft(), data.getRight(), learningRate, epochs,
                batchSize);
    }

    @Override
    public double getError(double[][] samples, double[][] labels) {
        double totalError = 0;

        for (int i = 0; i < samples.length; i++) {
            double[] h = predict(samples[i]);

            for (int j = 0; j < labels[0].length; j++) {
                double diff = labels[i][j] - layers.get(layers.size() - 1).getNeuron(j).getY();
                totalError += diff * diff;
            }
        }

        return totalError / (2 * samples.length);
    }

    @Override
    public double[] predict(double[] sample) {
        for (int i = 0; i < sample.length; i++) {
            layers.get(0).getNeuron(i).setW0(sample[i]);
        }

        for (int i = 1; i < layers.size(); i++) {
            layers.get(i).solveLayer(layers.get(i - 1));
        }

        ILayer last = layers.get(layers.size() - 1);
        double[] prediction = new double[last.size()];

        for (int i = 0; i < last.size(); i++) {
            prediction[i] = last.getNeuron(i).getY();
        }

        return prediction;
    }

    @Override
    public double[][] predict(double[][] samples) {
        double[][] predictions =
                new double[samples.length][layers.get(layers.size() - 1).size()];

        for (int i = 0; i < samples.length; i++) {
            predictions[i] = predict(samples[i]);
        }

        return predictions;
    }

    @Override
    public int predictLabel(double[] sample) {
        double[] prediction = predict(sample);

        return IntStream.range(0, prediction.length).
                reduce((i, j) -> prediction[i] < prediction[j] ? j : i).orElse(-1);
    }

    @Override
    public int[] predictLabels(double[][] samples) {
        int[] predictions = new int[samples.length];

        for (int i = 0; i < samples.length; i++) {
            predictions[i] = predictLabel(samples[i]);
        }

        return predictions;
    }

    private void updateWeights(double[][] samples, double[][] labels,
                               int batchSize, double learningRate) {

        int l = layers.size();

        for (int i = 0; i < samples.length; i++) {
            double[] prediction = predict(samples[i]);

            for (int j = 0; j < labels[0].length; j++) {
                //double delta = Math.pow(layers.get(outLayer).getNeuron(j).
                // getY() - (double) answers.get(i).get(j), 2);

                double y = layers.get(layers.size() - 1).getNeuron(j).getY();
                double delta = y * (1 - y) * (labels[i][j] - y);
                layers.get(l - 1).getNeuron(j).setDelta(delta);
            }

            for (int j = l - 2; j >= 0; j--) {
                layers.get(j).updateWeigths(layers.get(j + 1), learningRate);
                layers.get(j).updateDeltas(layers.get(j + 1));
            }

            if (i % batchSize == 0) {
                for (ILayer layer : layers)
                    layer.swapWeights();
            }
        }
    }

    @Override
    public Iterator<ILayer> iterator() {
        return layers.iterator();
    }

    @Override
    public void forEach(Consumer<? super ILayer> action) {
        layers.forEach(action::accept);
    }
}
