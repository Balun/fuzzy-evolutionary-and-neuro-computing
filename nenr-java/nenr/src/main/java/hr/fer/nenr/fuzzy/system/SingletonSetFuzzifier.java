package hr.fer.nenr.fuzzy.system;

import hr.fer.nenr.fuzzy.domain.Domain;
import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.set.MutableFuzzySet;

public class SingletonSetFuzzifier implements Fuzzifier {

    private Domain domain;

    public SingletonSetFuzzifier(Domain domain) {
        this.domain = domain;
    }

    @Override
    public IFuzzySet fuzzify(int value) {
        MutableFuzzySet set = new MutableFuzzySet(domain);
        set.set(DomainElement.of(value), 1);
        return set;
    }
}
