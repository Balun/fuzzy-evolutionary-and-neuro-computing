package hr.fer.nenr.fuzzy;

import hr.fer.nenr.fuzzy.domain.Domain;
import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.domain.IDomain;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.set.MutableFuzzySet;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.List;


public class Relations {

    public static boolean isSymmetric(IFuzzySet set) {
        if (!isUtimesURelation(set)) {
            return false;
        }

        for (DomainElement element : set.getDomain()) {
            int[] tmp = ArrayUtils.clone(element.getComponents());
            ArrayUtils.reverse(tmp);

            if (Math.abs(set.getValueAt(element) - set.getValueAt(DomainElement.of(tmp))) > 0.001) {
                return false;
            }
        }

        return true;
    }

    public static boolean isReflexive(IFuzzySet set) {
        if (!isUtimesURelation(set)) {
            return false;
        }

        for (DomainElement element : set.getDomain().getComponent(0)) {
            int value = element.getComponentValue(0);

            if (set.getValueAt(DomainElement.of(value, value)) != 1) {
                return false;
            }
        }

        return true;
    }

    public static boolean isMaxMinTransitive(IFuzzySet set) {
        if (!isUtimesURelation(set)) {
            return false;
        }

        IDomain simpleDomain = set.getDomain().getComponent(0);

        for (DomainElement element : set.getDomain()) {
            List<Double> minimums = new ArrayList<>();

            for (DomainElement simpleElement : simpleDomain) {
                DomainElement first =
                        DomainElement.of(element.getComponentValue(0),
                                simpleElement.getComponentValue(0));

                DomainElement second =
                        DomainElement.of(simpleElement.getComponentValue(0),
                                element.getComponentValue(1));

                minimums.add(Math.min(set.getValueAt(first),
                        set.getValueAt(second)));

            }

            if (set.getValueAt(element) < minimums.stream().max(Double::compareTo).orElse(0.)) {
                return false;
            }
        }

        return true;
    }

    public static IFuzzySet compositionOfBinaryRelations(IFuzzySet firstSet,
                                                         IFuzzySet secondSet) {
        if ((firstSet.getDomain().getNumberOfComponents() != 2) && (secondSet.getDomain().getNumberOfComponents() != 2)) {
            throw new IllegalArgumentException("Fuzzy sets must be consisted " +
                    "of two domains.");
        }

        if (!firstSet.getDomain().getComponent(1).equals(secondSet.getDomain().getComponent(0))) {
            throw new IllegalArgumentException("Relations must be of type UxV" +
                    " and VxW");
        }

        IDomain u = firstSet.getDomain().getComponent(0);
        IDomain v = firstSet.getDomain().getComponent(1);
        IDomain w = secondSet.getDomain().getComponent(1);

        MutableFuzzySet relation = new MutableFuzzySet(Domain.combine(u, w));

        for (DomainElement element : relation.getDomain()) {
            List<Double> minimums = new ArrayList<>();

            for (DomainElement simpleElement : v) {
                DomainElement first =
                        DomainElement.of(element.getComponentValue(0),
                                simpleElement.getComponentValue(0));

                DomainElement second =
                        DomainElement.of(simpleElement.getComponentValue(0),
                                element.getComponentValue(1));

                minimums.add(Math.min(firstSet.getValueAt(first),
                        secondSet.getValueAt(second)));

            }

            relation.set(element,
                    minimums.stream().max(Double::compareTo).orElse(0.));
        }

        return relation;
    }

    public static boolean isFuzzyEquivalence(IFuzzySet set) {
        return isSymmetric(set) && isReflexive(set) && isMaxMinTransitive(set);
    }

    public static boolean isUtimesURelation(IFuzzySet set) {
        IDomain domain = set.getDomain();

        if (domain.getNumberOfComponents() != 2) {
            return false;
        }

        IDomain comp1 = domain.getComponent(0);
        IDomain comp2 = domain.getComponent(1);

        return comp1.equals(comp2);
    }

}
