package hr.fer.nenr.fuzzy.set;

import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.domain.IDomain;

public class MutableFuzzySet implements IFuzzySet {

    private double[] memberships;

    private IDomain domain;

    public MutableFuzzySet(IDomain domain) {
        this.domain = domain;
        this.memberships = new double[domain.getCardinality()];
    }

    @Override
    public IDomain getDomain() {
        return this.domain;
    }

    @Override
    public double getValueAt(DomainElement element) {
        try {
            return memberships[domain.indexOfElement(element)];
        } catch (IndexOutOfBoundsException e) {
            return 0;
        }
    }

    public MutableFuzzySet set(DomainElement element, double value) {
        memberships[domain.indexOfElement(element)] = value;
        return this;
    }
}
