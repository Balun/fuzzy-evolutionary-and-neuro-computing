package hr.fer.nenr.fuzzy.set;

import hr.fer.nenr.fuzzy.utils.IIntUnaryFunction;

public class StandardFuzzySets {

    public static IIntUnaryFunction lFunction(int alpha, int beta) {
        return i -> {
            if (i < alpha) {
                return 1;

            } else if (i >= beta) {
                return 0;

            } else {
                return (double) (beta - i) / (beta - alpha);
            }
        };
    }

    public static IIntUnaryFunction gammaFunction(int alpha, int beta) {
        return i -> {
            if (i < alpha) {
                return 0;

            } else if (i < beta) {
                return (double) (i - alpha) / (beta - alpha);

            } else {
                return 1;
            }
        };
    }

    public static IIntUnaryFunction lambdaFunction(int alpha, int beta,
                                                   int gamma) {
        return i -> {
            if (i < alpha) {
                return 0;

            } else if (i < beta) {
                return (double) (i - alpha) / (beta - alpha);

            } else if (i < gamma) {
                return (double) (gamma - i) / (gamma - beta);

            } else {
                return 0;
            }
        };
    }
}
