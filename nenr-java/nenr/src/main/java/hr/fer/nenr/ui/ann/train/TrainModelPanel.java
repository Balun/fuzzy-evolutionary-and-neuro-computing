package hr.fer.nenr.ui.ann.train;

import hr.fer.nenr.ui.utils.SpringUtilities;

import javax.swing.*;
import java.awt.BorderLayout;

public class TrainModelPanel extends JPanel {

    private TrainModel model;

    public TrainModelPanel(TrainModel model) {
        setLayout(new BorderLayout());
        this.model = model;
        initGUI();
    }

    private void initGUI() {
        JPanel form = new JPanel(new SpringLayout());
        add(form, BorderLayout.CENTER);

        form.add(new JLabel("Dataset path: "));
        JTextField dataPath = new JTextField("src/main/resources/ann/final" +
                ".txt");
        form.add(dataPath);

        form.add(new JLabel("Epochs: "));
        JTextField epochs = new JTextField("10000");
        form.add(epochs);

        form.add(new JLabel("Hidden layer size: "));
        JTextField hiddenLayerSize = new JTextField("15");
        form.add(hiddenLayerSize);

        form.add(new JLabel("Number of layers: "));
        JTextField numOfLayers = new JTextField("4");
        form.add(numOfLayers);

        form.add(new JLabel("Learning rate"));
        JTextField learningRate = new JTextField("0.01");
        form.add(learningRate);

        form.add(new JLabel("Batch size: "));
        JTextField batchSize = new JTextField("1");
        form.add(batchSize);

        SpringUtilities.makeCompactGrid(form, 6, 2, 6, 6, 6, 6);

        JButton train = new JButton("Train!");
        add(train, BorderLayout.SOUTH);

        train.addActionListener(e -> model.train(dataPath.getText(),
                Integer.parseInt(epochs.getText()),
                Integer.parseInt(hiddenLayerSize.getText()),
                Integer.parseInt(numOfLayers.getText()),
                Double.parseDouble(learningRate.getText()),
                Integer.parseInt(batchSize.getText())));
    }

}
