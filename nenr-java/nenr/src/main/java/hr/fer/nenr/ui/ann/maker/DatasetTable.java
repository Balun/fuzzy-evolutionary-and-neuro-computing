package hr.fer.nenr.ui.ann.maker;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.awt.Color;

public class DatasetTable extends JPanel implements DataListener {

    private ClassificationMakerModel model;

    private JTable table;

    public DatasetTable(ClassificationMakerModel model) {
        this.model = model;
        this.model.addDataListener(this);

        this.table = new JTable(this.model);
        this.table.setAutoCreateColumnsFromModel(true);
        this.table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.table.setBackground(new Color(164, 178, 164));

        setLayout(new BorderLayout());
        add(this.table, BorderLayout.CENTER);

        JButton button = new JButton("New Class");
        button.addActionListener(e -> model.incrementClass());
        add(button, BorderLayout.NORTH);

        JButton clear = new JButton("Clear data");
        clear.addActionListener(e -> model.clearData());
        add(clear, BorderLayout.SOUTH);
    }

    @Override
    public void dataChanged() {
        this.table.revalidate();
        this.table.repaint();
    }

    public ClassificationMakerModel getModel() {
        return model;
    }

    public JTable getTable() {
        return table;
    }
}
