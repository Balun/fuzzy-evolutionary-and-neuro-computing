package hr.fer.nenr.fuzzy.utils;

public interface IIntUnaryFunction {

    double valueAt(int index);
}
