package hr.fer.nenr.fuzzy.domain;


import java.util.Arrays;

public class DomainElement {

    private int[] values;

    public DomainElement(int... elements) {
        this.values = elements;
    }

    public static DomainElement of(int... values) {
        return new DomainElement(values);
    }

    public static DomainElement combine(DomainElement first,
                                        DomainElement last) {

        int[] result = Arrays.copyOf(first.values,
                first.values.length + last.values.length);
        System.arraycopy(last.values, 0, result, first.values.length,
                last.values.length);

        return new DomainElement(result);
    }

    public int getNumberOfCompoments() {
        return values.length;
    }

    public int getComponentValue(int index) {
        return values[index];
    }

    public int[] getComponents() {
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainElement that = (DomainElement) o;
        return Arrays.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }

    @Override
    public String toString() {
        if (values.length == 1) {
            return Integer.toString(values[0]);

        } else {
            StringBuilder builder = new StringBuilder();
            builder.append("(");
            for (int elem : values) {
                builder.append(elem);
                builder.append(", ");
            }

            builder.replace(builder.length() - 2, builder.length(), ")");
            return builder.toString();
        }
    }

}