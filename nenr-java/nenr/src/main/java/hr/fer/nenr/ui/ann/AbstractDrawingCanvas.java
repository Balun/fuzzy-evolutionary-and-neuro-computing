package hr.fer.nenr.ui.ann;

import hr.fer.nenr.ui.ann.maker.ClassificationMakerModel;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDrawingCanvas extends JPanel {

    protected List<Point2D.Double> points;

    public AbstractDrawingCanvas() {
        this.setBackground(new Color(255, 255, 255));
        this.points = new ArrayList<>();

        this.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent evt) {
                canvasMousePressed(evt);
            }

            public void mouseReleased(MouseEvent evt) {
                canvasMouseReleased(evt);
            }

        });

        this.addMouseMotionListener(new MouseMotionAdapter() {


            public void mouseDragged(MouseEvent evt) {
                canvasMouseDragged(evt);
            }

        });
    }

    private void canvasMouseDragged(MouseEvent evt) {
        points.add(new Point2D.Double(evt.getX(), evt.getY()));
        this.repaint();
    }

    private void canvasMousePressed(MouseEvent evt) {
        this.points.clear();
        points.add(new Point2D.Double(evt.getX(), evt.getY()));
    }

    protected abstract void canvasMouseReleased(MouseEvent evt);

    public List<Point2D.Double> getPoints() {
        return points;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (!points.isEmpty()) {
            Point2D.Double from = points.get(0);
            for (int i = 1; i < points.size(); ++i) {
                Point2D.Double to = points.get(i);
                g.drawLine((int) from.x, (int) from.y, (int) to.x, (int) to.y);
                from = to;
            }
        }
    }
}
