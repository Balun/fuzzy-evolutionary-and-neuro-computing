package hr.fer.nenr.fuzzy.domain;

import java.util.Iterator;
import java.util.Objects;

public class SimpleDomain extends Domain {

    private int first;

    private int last;

    public SimpleDomain(int first, int last) {
        if (first >= last) {
            throw new IllegalArgumentException("Error in constraints");
        }

        this.first = first;
        this.last = last;
    }

    public int getFirst() {
        return first;
    }

    public int getLast() {
        return last;
    }

    @Override
    public int getCardinality() {
        return last - first;
    }

    @Override
    public IDomain getComponent(int index) {
        throw new UnsupportedOperationException("Simle domain has no " +
                "components");
    }

    @Override
    public int getNumberOfComponents() {
        return 1;
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return new Iterator<DomainElement>() {

            private int current = first;

            @Override
            public boolean hasNext() {
                return current < last;
            }

            @Override
            public DomainElement next() {
                return DomainElement.of(current++);
            }
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleDomain that = (SimpleDomain) o;
        return first == that.first &&
                last == that.last;
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, last);
    }
}
