package hr.fer.nenr.ui.ann.train;

import hr.fer.nenr.ann.Utils;
import hr.fer.nenr.ann.layers.Dense;
import hr.fer.nenr.ann.layers.InputLayer;
import hr.fer.nenr.ann.models.INeuralNet;
import hr.fer.nenr.ann.models.Sequential;
import hr.fer.nenr.ui.ann.maker.DataListener;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

public class TrainModel {

    private INeuralNet net;

    private double[] currentPrediction;

    private List<DataListener> listeners;

    public TrainModel(INeuralNet net) {
        this.net = net;
        this.listeners = new ArrayList<>();
    }

    public TrainModel() {
        this(null);
    }

    public boolean isTrained() {
        return net != null;
    }

    public INeuralNet train(String dataPath, int epochs, int hiddenLayerSize,
                            int numLayers, double learningRate, int batchSize) {

        if (numLayers < 3) {
            throw new IllegalArgumentException("The model must have at least " +
                    "one hidden layer");
        }

        Pair<double[][], double[][]> data =
                Utils.dataFromFile(dataPath);

        double[][] samples = data.getLeft();
        double[][] labels = data.getRight();

        net = new Sequential().add(new InputLayer(samples[0].length,
                hiddenLayerSize));

        for (int i = 0; i < numLayers - 3; i++) {
            net.add(new Dense(hiddenLayerSize, hiddenLayerSize));
        }

        net.add(new Dense(hiddenLayerSize, labels[0].length));
        net.add(new Dense(labels[0].length, 0));

        net.fit(samples, labels, learningRate, epochs, batchSize);

        return net;
    }

    public double[] predict(double[] sample) {
        if (!isTrained()) {
            throw new UnsupportedOperationException("The model has not yet " +
                    "been trained.");
        }

        currentPrediction = net.predict(sample);
        fire();
        return currentPrediction;
    }

    public double[] getCurrentPrediction() {
        if (currentPrediction == null) {
            throw new UnsupportedOperationException("The model has not yet " +
                    "predicted anything.");
        }

        return currentPrediction;
    }

    private void fire() {
        listeners.forEach(DataListener::dataChanged);
    }

    public void addDataListener(DataListener l) {
        listeners.add(l);
    }

    public void removeDataListener(DataListener l) {
        listeners.remove(l);
    }
}
