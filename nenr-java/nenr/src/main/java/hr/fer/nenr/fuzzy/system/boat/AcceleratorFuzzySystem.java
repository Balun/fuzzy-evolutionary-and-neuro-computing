package hr.fer.nenr.fuzzy.system.boat;

import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.operations.Operations;
import hr.fer.nenr.fuzzy.set.CalculatedFuzzySet;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.system.Defuzzifier;
import hr.fer.nenr.fuzzy.system.FuzzySystem;
import hr.fer.nenr.fuzzy.system.rule.Base;
import hr.fer.nenr.fuzzy.utils.IBinaryFunction;

import java.util.List;

import static hr.fer.nenr.fuzzy.set.StandardFuzzySets.*;
import static hr.fer.nenr.fuzzy.system.boat.BoatUtils.*;

public class AcceleratorFuzzySystem extends FuzzySystem {

    public static final IBinaryFunction S_NORM = Operations.zadehOr();

    public static final IBinaryFunction T_NORM = Operations.zadehAnd();

    public static final IBinaryFunction IMPLICATION = Operations.zadehAnd();

    private static final IFuzzySet SLOW =
            new CalculatedFuzzySet(velocityDomain(),
                    lFunction(velocityDomain().indexOfElement(DomainElement.of(70)),
                            velocityDomain().indexOfElement(DomainElement.of(100))));

    private static final IFuzzySet FAST =
            new CalculatedFuzzySet(velocityDomain(),
                    gammaFunction(velocityDomain().indexOfElement(DomainElement.of(60)),
                            velocityDomain().indexOfElement(DomainElement.of(90))));

    private static final IFuzzySet WALL_CLOSE =
            new CalculatedFuzzySet(distanceDomain(),
                    lFunction(distanceDomain().indexOfElement(DomainElement.of(45)),
                            distanceDomain().indexOfElement(DomainElement.of(85))));

    private static final IFuzzySet WALL_CLOSE_WITH_ANGLE =
            new CalculatedFuzzySet(distanceDomain(),
                    lFunction(distanceDomain().indexOfElement(DomainElement.of(60)),
                            distanceDomain().indexOfElement(DomainElement.of(180))));

    private static final IFuzzySet DECREASE_SPEED =
            new CalculatedFuzzySet(accelerationDomain(),
                    lambdaFunction(accelerationDomain().indexOfElement(DomainElement.of(-15)),
                            accelerationDomain().indexOfElement(DomainElement.of(-10)), accelerationDomain().
                                    indexOfElement(DomainElement.of(-5))));

    private static final IFuzzySet DECREASE_SPEED_HARD =
            new CalculatedFuzzySet(accelerationDomain(),
                    lambdaFunction(accelerationDomain().indexOfElement(DomainElement.of(-25)),
                            accelerationDomain().indexOfElement(DomainElement.of(-20)), accelerationDomain().
                                    indexOfElement(DomainElement.of(-15))));

    private static final IFuzzySet INCREASE_SPEED =
            new CalculatedFuzzySet(accelerationDomain(),
                    lambdaFunction(accelerationDomain().indexOfElement(DomainElement.of(5)),
                            accelerationDomain().indexOfElement(DomainElement.of(10)), accelerationDomain().
                                    indexOfElement(DomainElement.of(15))));

    private static final IFuzzySet INCREASE_SPEED_HARD =
            new CalculatedFuzzySet(accelerationDomain(),
                    lambdaFunction(accelerationDomain().indexOfElement(DomainElement.of(15)),
                            accelerationDomain().indexOfElement(DomainElement.of(20)), accelerationDomain().
                                    indexOfElement(DomainElement.of(25))));

    public static Base ACCELERATOR_RULE_BASE;

    static {
        ACCELERATOR_RULE_BASE = new Base(S_NORM, T_NORM, IMPLICATION);

        ACCELERATOR_RULE_BASE.add(List.of(distanceSet(), distanceSet(), distanceSet(), distanceSet(), FAST, directionSet()), DECREASE_SPEED);

        ACCELERATOR_RULE_BASE.add(List.of(distanceSet(), distanceSet(),
                distanceSet(), distanceSet(), SLOW, directionSet()),
                INCREASE_SPEED);

        ACCELERATOR_RULE_BASE.add(List.of(WALL_CLOSE, distanceSet(),
                distanceSet(), distanceSet(), SLOW, directionSet()),
                INCREASE_SPEED);

        ACCELERATOR_RULE_BASE.add(List.of(WALL_CLOSE, distanceSet(),
                distanceSet(), distanceSet(), SLOW, directionSet()),
                INCREASE_SPEED);

        ACCELERATOR_RULE_BASE.add(List.of(WALL_CLOSE, distanceSet(),
                distanceSet(), distanceSet(), FAST, directionSet()),
                DECREASE_SPEED);

        ACCELERATOR_RULE_BASE.add(List.of(WALL_CLOSE, distanceSet(),
                distanceSet(), distanceSet(), FAST, directionSet()),
                DECREASE_SPEED);


        ACCELERATOR_RULE_BASE.add(List.of(distanceSet(), distanceSet(), WALL_CLOSE_WITH_ANGLE, distanceSet(), velocitySet(), directionSet()), DECREASE_SPEED);

        ACCELERATOR_RULE_BASE.add(List.of(distanceSet(), distanceSet(), distanceSet(), WALL_CLOSE_WITH_ANGLE, velocitySet(), directionSet()), DECREASE_SPEED);
    }

    public AcceleratorFuzzySystem(Defuzzifier def) {
        super(ACCELERATOR_RULE_BASE, def);
    }
}
