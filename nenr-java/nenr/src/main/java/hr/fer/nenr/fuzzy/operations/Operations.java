package hr.fer.nenr.fuzzy.operations;

import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.set.IFuzzySet;
import hr.fer.nenr.fuzzy.set.MutableFuzzySet;
import hr.fer.nenr.fuzzy.utils.IBinaryFunction;
import hr.fer.nenr.fuzzy.utils.IUnaryFunction;


public class Operations {

    private static final IUnaryFunction ZADEH_NOT = x -> 1 - x;

    private static final IBinaryFunction ZADEH_OR = Math::max;

    private static final IBinaryFunction ZADEH_AND = Math::min;

    private static final IBinaryFunction HAMACHER_AND =
            (a, b) -> (a * b) / (a + b - a * b);

    private static final IBinaryFunction HAMACHER_OR =
            (a, b) -> (a + b - 2 * a * b) / (1 - a * b);

    public static IFuzzySet unaryOperation(IFuzzySet set,
                                           IUnaryFunction function) {

        MutableFuzzySet result = new MutableFuzzySet(set.getDomain());

        for (DomainElement element : set.getDomain()) {
            result.set(element, function.valueAt(set.getValueAt(element)));
        }

        return result;
    }

    public static IFuzzySet binaryOperation(IFuzzySet firstSet, IFuzzySet secondSet,
                                            IBinaryFunction function) {

        if (firstSet.getDomain().getCardinality() != secondSet.getDomain().getCardinality()) {
            throw new IllegalArgumentException("The specified sets are not of" +
                    " the same cardinality");
        }

        MutableFuzzySet result = new MutableFuzzySet(firstSet.getDomain());

        for (DomainElement el : result.getDomain()) {
            result.set(el, function.valueAt(firstSet.getValueAt(el),
                    secondSet.getValueAt(el)));
        }

        return result;
    }

    public static IUnaryFunction zadehNot() {
        return ZADEH_NOT;
    }

    public static IBinaryFunction zadehOr() {
        return ZADEH_OR;
    }

    public static IBinaryFunction zadehAnd() {
        return ZADEH_AND;
    }

    public static IBinaryFunction hamacherTNorm(double v) {
        return (x, y) -> x * y / (v + (1 - v) * (x + y - x * y));
    }

    public static IBinaryFunction hamacherSNorm(double v) {
        return (x, y) -> (x + y - (2 - v) * x * y) / (1 - (1 - v) * x * y);
    }
}
