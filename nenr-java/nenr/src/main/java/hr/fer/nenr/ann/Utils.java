package hr.fer.nenr.ann;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.awt.geom.Point2D;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.Function;

public class Utils {

    public static final int M = 20;

    public static final Function<Double, Double> SIGMOID = x ->
            1. / (1 + Math.exp(-x));

    public static Pair<double[][], int[][]> transfromDataFromMap(Map<Integer,
            List<List<Point2D.Double>>> data) {

        int classCount = data.size();
        int sampleCount = classCount * data.get(0).size();

        int[][] labels = new int[sampleCount][classCount];
        double[][] samples = new double[sampleCount][2 * M];

        int i = 0;
        for (Map.Entry<Integer, List<List<Point2D.Double>>> entry :
                data.entrySet()) {
            int classIndex = entry.getKey();

            for (List<Point2D.Double> sample : entry.getValue()) {
                samples[i] = transformSample(sample);
                labels[i++] = transformLabel(classIndex, classCount);
            }
        }

        return new ImmutablePair<>(samples, labels);
    }

    public static String dataToString(Pair<double[][], int[][]> data) {
        int n = data.getLeft().length;
        double[][] samples = data.getLeft();
        int[][] labels = data.getRight();
        StringBuilder builder = new StringBuilder();

        builder.append(n).append(" ").append(samples[0].length).append(" ").
                append(labels[0].length).append("\n");

        for (int i = 0; i < n; i++) {
            builder.append(sampleToString(samples[i]));
            builder.append(" ");
            builder.append(labelToString(labels[i]));
            builder.append("\n");
        }

        return builder.toString();
    }

    public static Pair<double[][], double[][]> dataFromFile(String filePath) {

        try (Scanner sc = new Scanner(new FileInputStream(filePath))) {
            double[][] samples = {};
            double[][] labels = {};
            int N = 0;
            int c = 0;
            int n = 0;

            if (sc.hasNextLine()) {
                String firstLine = sc.nextLine();
                N = Integer.parseInt(firstLine.split(" ")[0]);
                n = Integer.parseInt(firstLine.split(" ")[1]);
                c = Integer.parseInt(firstLine.split(" ")[2]);

                samples = new double[N][n];
                labels = new double[N][c];
            }

            for (int i = 0; sc.hasNextLine() && (i < N); i++) {
                String line = sc.nextLine();

                samples[i] =
                        Arrays.stream(Arrays.copyOfRange(line.split(" "), 0,
                                n)).mapToDouble(Double::
                                parseDouble).toArray();

                labels[i] =
                        Arrays.stream(Arrays.copyOfRange(line.split(" "), n,
                                n + c)).mapToDouble(Double::
                                parseDouble).toArray();
            }

            return new ImmutablePair<>(samples, labels);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        System.err.println("error in reading input file");
        System.exit(-1);
        return null;
    }

    public static void writeToFile(Pair<double[][], int[][]> data,
                                   String fileName) throws IOException {

        try (PrintWriter out = new PrintWriter(fileName)) {
            out.print(dataToString(data));
        }
    }

    public static double[] transformSample(List<Point2D.Double> sample) {
        List<Point2D.Double> scaled = scaleSample(sample);

        double dist = getGestureLength(scaled);
        double block = dist / (M - 1);
        double nextDist = block;
        double currDist = 0;

        List<Point2D.Double> fixedSample = new ArrayList<>();
        fixedSample.add(scaled.get(0));

        for (int i = 1; i < scaled.size(); ++i) {
            currDist += getDist(scaled.get(i - 1), scaled.get(i));
            if (currDist >= nextDist) {
                nextDist += block;
                fixedSample.add(scaled.get(i));
            }
        }

        while (fixedSample.size() != M) {
            fixedSample.add(scaled.get(scaled.size() - 1));
        }

        return getAsArray(fixedSample);
    }

    public static List<Point2D.Double> scaleSample(List<Point2D.Double> sample) {
        List<Point2D.Double> ret = new ArrayList<>();

        double xMean =
                sample.stream().mapToDouble(Point2D.Double::getX).average().orElse(Double.NaN);
        double yMean =
                sample.stream().mapToDouble(Point2D.Double::getY).average().orElse(Double.NaN);

        double mX = 0, mY = 0;

        for (Point2D.Double p : sample) {
            Point2D.Double newPoint = new Point2D.Double(p.getX() - xMean,
                    p.getY() - yMean);
            ret.add(newPoint);

            mX = Math.max(mX, Math.abs(newPoint.x));
            mY = Math.max(mY, Math.abs(newPoint.y));
        }

        double m = Math.max(mX, mY);
        ret.forEach(p -> p.setLocation(p.x / m, p.y / m));

        return ret;
    }

    public static double getGestureLength(List<Point2D.Double> sample) {
        double dist = 0;

        for (int i = 1; i < sample.size(); i++) {
            dist += getDist(sample.get(i), sample.get(i - 1));
        }

        return dist;
    }

    public static double[] getAsArray(List<Point2D.Double> sample) {
        double[] ret = new double[sample.size() * 2];

        for (int i = 0, j = 0; i < sample.size(); i++, j += 2) {
            ret[j] = sample.get(i).getX();
            ret[j + 1] = sample.get(i).getY();
        }

        return ret;
    }

    public static int[] transformLabel(int label, int n) {
        int[] ret = new int[n];
        ret[label] = 1;
        return ret;
    }

    public static String sampleToString(double[] sample) {
        StringBuilder builder = new StringBuilder();
        for (double e : sample) {
            builder.append(e);
            builder.append(" ");
        }

        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    public static String labelToString(int[] label) {
        StringBuilder builder = new StringBuilder();
        for (int e : label) {
            builder.append(e);
            builder.append(" ");
        }

        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    public static double getDist(Point2D.Double A, Point2D.Double B) {
        return Math.sqrt((A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y));
    }

    public static String getLetterByIndex(int i) {
        switch (i) {
            case 0:
                return "Alpha";

            case 1:
                return "Beta";

            case 2:
                return "Gamma";

            case 3:
                return "Delta";

            case 4:
                return "Epsilon";

            default:
                throw new IllegalArgumentException("Greek alphabet index out " +
                        "of bounds");
        }
    }
}
