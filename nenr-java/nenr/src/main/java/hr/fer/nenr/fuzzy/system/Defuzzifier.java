package hr.fer.nenr.fuzzy.system;

import hr.fer.nenr.fuzzy.set.IFuzzySet;

public interface Defuzzifier {

    int defuzzify(IFuzzySet set);
}
