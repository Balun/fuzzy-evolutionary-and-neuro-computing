package hr.fer.nenr.ui.ann.maker;

import hr.fer.nenr.ann.Utils;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;

public class ClassificationMakerPanel extends JPanel {


    public ClassificationMakerPanel(ClassificationMakerModel<List<Point2D.Double>> model) {
        setLayout(new BorderLayout());
        add(new ClassificationDrawingCanvas(model), BorderLayout.CENTER);
        add(new DatasetTable(model), BorderLayout.EAST);

        JButton saveButton = new JButton("Save dataset");
        saveButton.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser(".");

            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                String savePath =
                        chooser.getSelectedFile().toPath().toAbsolutePath().toString();
                try {
                    Utils.writeToFile(Utils.transfromDataFromMap(model.getData()),
                            savePath);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        add(saveButton, BorderLayout.SOUTH);
    }
}
