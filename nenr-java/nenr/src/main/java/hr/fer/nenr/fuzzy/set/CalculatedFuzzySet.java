package hr.fer.nenr.fuzzy.set;

import hr.fer.nenr.fuzzy.domain.DomainElement;
import hr.fer.nenr.fuzzy.domain.IDomain;
import hr.fer.nenr.fuzzy.utils.IIntUnaryFunction;

public class CalculatedFuzzySet implements IFuzzySet {

    private IDomain domain;

    private IIntUnaryFunction function;

    public CalculatedFuzzySet(IDomain domain, IIntUnaryFunction function) {
        this.domain = domain;
        this.function = function;
    }

    @Override
    public IDomain getDomain() {
        return domain;
    }

    @Override
    public double getValueAt(DomainElement element) {
        return function.valueAt(domain.indexOfElement(element));
    }
}
