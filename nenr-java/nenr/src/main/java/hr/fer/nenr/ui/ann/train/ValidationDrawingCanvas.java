package hr.fer.nenr.ui.ann.train;

import hr.fer.nenr.ann.Utils;
import hr.fer.nenr.ui.ann.AbstractDrawingCanvas;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

public class ValidationDrawingCanvas extends AbstractDrawingCanvas {

    private TrainModel model;

    public ValidationDrawingCanvas(TrainModel model) {
        this.model = model;
    }

    @Override
    protected void canvasMouseReleased(MouseEvent evt) {
        points.add(new Point2D.Double(evt.getX(), evt.getY()));
        this.repaint();

        model.predict(Utils.transformSample(points));
    }
}
