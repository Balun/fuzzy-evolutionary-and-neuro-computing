package hr.fer.nenr;

import hr.fer.nenr.ann.Utils;
import hr.fer.nenr.ann.layers.Dense;
import hr.fer.nenr.ann.layers.InputLayer;
import hr.fer.nenr.ann.models.INeuralNet;
import hr.fer.nenr.ann.models.Sequential;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;

public class ANNTests {

    public static void main(String[] args) {
        test1();
    }

    public static void test1() {
        Pair<double[][], double[][]> data =
                Utils.dataFromFile(System.getProperty("user.dir") + "/src" +
                        "/main" +
                        "/resources/ann/dataset.txt");

        double[][] samples = data.getLeft();
        double[][] labels = data.getRight();

        INeuralNet net = new Sequential();

        net.add(new InputLayer(samples[0].length, 10))
                .add(new Dense(10, 10))
                .add(new Dense(10, labels[0].length))
                .add(new Dense(labels[0].length, 0));

        net = net.fit(samples, labels, 0.01, 10000, 30);

        double[] prediction = net.predict(samples[0]);
        double[] real = labels[0];
    }

    public static void test2() {
        double[][] samples = {{-1}, {-0.8}, {-0.6}, {-0.4}, {-0.2}, {0}, {0.2}
                , {0.4}, {0.6}, {0.8}, {1}};
        double[][] labels = {{1}, {0.64}, {0.36}, {0.16}, {0.04}, {0}, {0.04}
                , {0.16}, {0.36}, {0.64}, {1}};

        INeuralNet net = new Sequential();
        net.add(new InputLayer(samples[0].length, 6))
                .add(new Dense(6, 1, x -> x))
                .add(new Dense(1, 0, x -> x));

        net = net.fit(samples, labels, 0.01, 20, 1);

        double[][] test = {{1}, {2}, {3}, {4}};

        double[][] res = net.predict(test);
        System.out.println(Arrays.toString(res));
    }
}
