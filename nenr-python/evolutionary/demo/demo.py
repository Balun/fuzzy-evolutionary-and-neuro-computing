import src.genetic.genetic_algorithm as genetic
import src.genetic.chromosome as chromosome
import src.genetic.utils as utils
import os


def generation_without_elitism():
    # TODO
    pass


def generation_with_elitism():
    # TODO
    pass


def elimination():
    # TODO
    pass


if __name__ == '__main__':
    choice = int(input("Select option:"))

    if choice == 0:
        generation_without_elitism()

    elif choice == 1:
        generation_with_elitism()

    elif choice == 2:
        elimination()

    else:
        raise ValueError("Incorrect choice.")
