"""

"""
import random


def tour_selection(population, k=3):
    """

    :param population:
    :param k:
    :return:
    """
    return random.choices(population, k=k)


def roulette_wheel(population, k=3):
    """

    :param population:
    :param k:
    :return:
    """
    errors = [tmp.error for tmp in population]

    min_value = min(errors)
    max_value = max(errors)

    errors = (1 - (x - min_value) / (max_value - min_value) for x in errors)

    if k == 1:
        return population[_get_roulette_index(errors)]

    selection = []

    while len(selection) < 3:
        tmp = population[_get_roulette_index(errors)]

        if tmp not in selection:
            selection.append(tmp)

    return selection


def _get_roulette_index(weights):
    """

    :param weights:
    :return:
    """
    value = random.uniform(0, sum(weights))

    for i, weight in enumerate(weights):
        value -= weight

        if value < 0:
            return i

    return weights[-1]