"""

"""
import abc
import numpy as np

import src.genetic.selections as selections
import src.genetic.crossovers as crossovers
import src.genetic.utils as utils


class GeneticAlgorithm(abc.ABC):
    """

    """

    def __init__(self, data, max_iter=400, min_error=1e-6, k=3):
        """

        :param pop_size:
        :param max_iter:
        :param min_error:
        """
        self.max_iter = max_iter
        self.min_error = min_error
        self.data = data

        self.pop_size = len(data)
        self.population = sorted(self.data)
        self._errors = []
        self.k = k

    def run(self, trace=False):
        """

        :param data:
        :return:
        """
        error = 0

        for i in range(self.max_iter):
            best = self.population[0]
            error = self.population[0].error()

            if abs(error) < self.min_error:
                return error, self.population[0].data

            self.population = sorted(self.get_next_generation())

            if self.population[0] < best and trace:
                self._errors.append([i, error])
                print("Iteration " + str(i) + ", error=" + str(error) + ", "
                                                                        "new best solution: " +
                      str(self.population[0].eval()))

            elif self.population[0] < best:
                self._errors.append([i, error])

        return error, self.population[0].eval()

    @abc.abstractmethod
    def get_next_generation(self):
        """

        :return:
        """
        raise NotImplementedError()

    @property
    def errors(self):
        """

        :return:
        """
        return self._errors


class EliminationGeneticAlgorithm(GeneticAlgorithm):
    """

    """

    def get_next_generation(self):
        """

        :return:
        """
        tour = sorted(selections.tour_selection(self.population, self.k),
                      reverse=True)
        self.population.remove(tour[0])
        new_chromosome = crossovers.uniform_crossover(tour[-1], tour[-2])
        new_chromosome.mutate()
        self.population.append(new_chromosome)

        return self.population


class GenerationGeneticAlgorithm(GeneticAlgorithm):
    """

    """

    def __init__(self, data, elitism=True, max_iter=400, min_error=1e-6,
                 k=3):
        super().__init__(data, max_iter, min_error, k)

        self.elitism = elitism

    def get_next_generation(self):
        """

        :return:
        """
        tmp_population = []
        for i in range(len(self.population)):
            first = self._get_parent()
            second = self._get_parent()
            tmp_population.append(crossovers.uniform_crossover(first, second))
            tmp_population[i].mutate()

        if self.elitism:
            tmp_population.insert(0, self.population[0])

        return tmp_population

    def _get_parent(self):
        """

        :return:
        """
        error_sum = 0
        max_error = 0

        for e in self.population:
            error_sum += e.error()
            max_error = max(max_error, e.error())

        cut_tresh = np.random.uniform(0, len(self.population) * max_error -
                                      error_sum)

        error_sum = 0
        for e in self.population:
            error_sum += max_error - e.error()

            if error_sum > cut_tresh:
                return e

        return self.population[-1]
