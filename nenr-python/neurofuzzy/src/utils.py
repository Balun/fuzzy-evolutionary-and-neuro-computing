"""

"""
import math
import numpy as np


class Sample:
    """
    """

    def __init__(self, x, y, f=None):
        self.x = x
        self.y = y
        self.z = f(x, y) if f else func(x, y)


def func(x, y):
    """

    :param x:
    :param y:
    :return:
    """
    return ((x - 1) ** 2 + (y + 2) ** 2 - 5 * x * y + 3) * math.pow(math.cos(x
                                                                             /
                                                                             5),
                                                                    2)


def generate_data(l=-4, u=4, f=None):
    """

    :param l:
    :param u:
    :param f:
    :return:
    """
    data = []
    f = f if f else func

    for x in range(l, u + 1):
        for y in range(l, u + 1):
            data.append(Sample(x, y, f))

    return data


def sigmoid(x, a, b):
    """

    :param x:
    :param a:
    :param b:
    :return:
    """
    return 1. / (1 + np.exp(b * (x - a)))


def t_norm(u_a, u_b):
    """

    :param u_a:
    :param u_b:
    :return:
    """
    return u_a * u_b
