"""
"""

from src.utils import *
from src.rule import Rule
import random


class ANFIS:
    """
    """

    def __init__(self, learning_rate=0.01, max_iter=100000, n_rules=7):
        """

        :param learning_rate:
        :param max_iter:
        :param n_rules:
        """
        self.learning_rate = learning_rate
        self.max_iter = max_iter

        self._rules = [Rule() for _ in range(n_rules)]

        self._data = generate_data()

        self.sum_w = self.sum_wz = 0

    def eval(self, x, y):
        """

        :return:
        """
        self.sum_wz = 0
        self.sum_w = 0

        for r in self.rules:
            w = r.conclude(x, y)

            self.sum_w += w
            self.sum_wz += w * r.eval(x, y)

        return self.sum_wz / self.sum_w

    @property
    def errors(self):
        """

        :return:
        """
        return [abs(s.z - self.eval(s.x, s.y)) for s in self.data]

    @property
    def mean_squared_error(self):
        """

        :return:
        """
        res = 0

        for s in self.data:
            res += pow(self.eval(s.x, s.y) - s.z, 2)

        return res / len(self.data)

    @property
    def rules(self):
        """

        :return:
        """
        return self._rules

    @property
    def data(self):
        """

        :return:
        """
        return self._data

    def fit_batch(self, trace=False):
        """

        :return:
        """
        errors = []

        for i in range(self.max_iter):

            for s in self.data:
                o = self.eval(s.x, s.y)

                for r in self.rules:
                    z = r.eval(s.x, s.y)
                    r.update_derivatives(s, o, self.sum_w,
                                         self.sum_w * z - self.sum_wz)

            for r in self.rules:
                r.update(self.learning_rate)

            if i % 500 == 0:
                errors.append(self.mean_squared_error)

            if i % 500 == 0 and trace:
                print(
                    "epoch -> %d, error -> %f" % (i, self.mean_squared_error))

        return np.array(errors)

    def fit_stohastic(self, trace=False):
        """

        :return:
        """
        errors = []

        for i in range(self.max_iter):
            if i % len(self.data) == 0:
                random.shuffle(self.data)

            s = self.data[i % len(self.data)]

            o = self.eval(s.x, s.y)

            for r in self.rules:
                z = r.eval(s.x, s.y)
                r.update_derivatives(s, o, self.sum_w, self.sum_w * z -
                                     self.sum_wz)
                r.update(self.learning_rate)

            if i % 500 == 0:
                errors.append(self.mean_squared_error)

            if i % 500 == 0 and trace:
                print(
                    "epoch -> %d, error -> %f" % (i, self.mean_squared_error))

        return np.array(errors)
