from src import anfis


def first():
    model = anfis.ANFIS()
    model.fit_batch()


if __name__ == '__main__':
    first()
