"""
"""
import random

from src.utils import *


class Rule:
    """
    """

    def __init__(self):
        """
        """
        self.a = random.uniform(-1, 1)
        self.b = random.uniform(-1, 1)
        self.c = random.uniform(-1, 1)
        self.d = random.uniform(-1, 1)

        self.p = random.uniform(-1, 1)
        self.q = random.uniform(-1, 1)
        self.r = random.uniform(-1, 1)

        self.d_a = self.d_b = self.d_c = self.d_d = self.d_p = self.d_q = \
            self.d_r = 0

    def eval(self, x, y):
        """

        :param x:
        :return:
        """
        return self.p * x + self.q * y + self.r

    def conclude(self, x, y):
        """

        :return:
        """
        return sigmoid(x, self.a, self.b) * sigmoid(y, self.c, self.d)

    def update_derivatives(self, sample, o, sum_w, sum_wz):
        """

        :param x:
        :param o:
        :param sum_w:
        :param sum_wz:
        :return:
        """
        w = self.conclude(sample.x, sample.y)
        alpha = sigmoid(sample.x, self.a, self.b)
        beta = sigmoid(sample.y, self.c, self.d)

        self.d_p += (sample.z - o) * (w / sum_w) * sample.x
        self.d_q += (sample.z - o) * (w / sum_w) * sample.y
        self.d_r += (sample.z - o) * (w / sum_w)

        self.d_a += (sample.z - o) * (sum_wz / (
                sum_w * sum_w)) * beta * self.b * alpha * (1 - alpha)

        self.d_b += (sample.z - o) * sum_wz / (sum_w * sum_w) * beta * (
                self.a - sample.x) * alpha * (1 - alpha)

        self.d_c += (sample.z - o) * sum_wz / (
                sum_w * sum_w) * alpha * self.d * beta * (1 - beta)

        self.d_d += (sample.z - o) * sum_wz / (sum_w * sum_w) * alpha * (
                self.c - sample.y) * beta * (1 - beta)

    def update(self, eta):
        """

        :return:
        """
        self.p += eta * self.d_p
        self.q += eta * self.d_q
        self.r += eta * self.d_r

        self.a += eta * self.d_a
        self.b += eta * self.d_b
        self.c += eta * self.d_c
        self.d += eta * self.d_d

        self.d_a = self.d_b = self.d_c = self.d_d = self.d_p = self.d_q = self.d_r = 0
