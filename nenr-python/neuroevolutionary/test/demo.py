import src.genetic.chromosome as chromosome
import src.neural.models as models
import src.utils as utils
import src.genetic.genetic_algorithm as genetic

X, y = utils.read_data('../res/dataset.txt')

net = models.NeuralNet(2, 8, 3)

population = chromosome.generate_new_float_population(30,
                                                      net.get_params_size(),
                                                      lambda
                                                          params:
                                                      net.error(X,
                                                                y,
                                                                params),
                                                      -1, 1)

a = population[0]
gen = genetic.EliminationGeneticAlgorithm(population)
params, error, errors = gen.run(True)

print("Error:", error)

h = net.predict(X, params)

hits = 0

for a, b in zip(y, h):
    if a == b:
        hits += 1

print("hits: %d/%d" % (hits, len(y)))
