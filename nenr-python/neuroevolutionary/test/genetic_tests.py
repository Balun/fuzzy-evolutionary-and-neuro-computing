import unittest as ut
import src.genetic.chromosome as chromosome
import src.genetic.crossovers as crossovers
import src.neural.models as models
import src.utils as utils
import src.genetic.genetic_algorithm as genetic


class TestGeneticMethods(ut.TestCase):

    def test_1_point_crossover(self):
        first = chromosome.generate_new_float_chromosome(5, lambda x: sum(x))
        second = chromosome.generate_new_float_chromosome(5, lambda x: sum(x))

        child = crossovers.t_point_crossover(first, second, 1)
        print(child.data)

    def test_2_point_crossover(self):
        first = chromosome.generate_new_float_chromosome(5, lambda x: sum(x))
        second = chromosome.generate_new_float_chromosome(5, lambda x: sum(x))

        child = crossovers.t_point_crossover(first, second, 2)
        print(child.data)

    def test_gen_algorithm(self):
        X, y = utils.read_data('../res/dataset.txt')

        net = models.NeuralNet(2, 8, 3)

        def error_function(net, params, X, y):
            return net.error(X, y, params)

        population = chromosome.generate_new_float_population(50,
                                                              net.get_params_size(),
                                                              lambda
                                                                  params:
                                                              net.error(X,
                                                                        y,
                                                                        params),
                                                              -1, 1)

        a = population[0]
        gen = genetic.EliminationGeneticAlgorithm(population)
        params, error, errors = gen.run()

    def test_predict(self):
        X, y = utils.read_data('../res/dataset.txt')

        net = models.NeuralNet(2, 8, 3)

        population = chromosome.generate_new_float_population(50,
                                                              net.get_params_size(),
                                                              lambda
                                                                  params:
                                                              net.error(X,
                                                                        y,
                                                                        params),
                                                              -1, 1)

        gen = genetic.EliminationGeneticAlgorithm(population)
        params, error, errors = gen.run(True)

        h = net.predict(X, params)

        hits = 0

        for a, b in zip(y, h):
            if a == b:
                hits += 1

        print("hits: %d/%d" % (hits, len(y)))
