import numpy as np
import matplotlib.pyplot as plt

GENERATIONS = 10000
POP_SIZE = 50

P = 0.1

SIGMA_1 = 0.3
SIGMA_2 = 0.3


def read_data(file_path='res/dataset.txt'):
    """

    :param file_path:
    :return:
    """
    X = []
    y = []
    with open(file_path) as fp:
        for line in fp:
            params = line.split('\t', 2)
            X.append(np.array([float(params[0]), float(params[1])]))
            y.append(''.join(params[2].strip().split()).find('1'))

    return np.array(X), np.array(y)


def choose_index(probs):
    if np.sum(probs) == 0:
        raise ValueError("sum of weights must be greater than zero.")

    v = np.array([t / np.sum(probs) for t in probs])
    return np.random.choice(len(v), 1, p=v)[0]


def f1(x):
    return 1 / (1 + x)


def f2(x):
    return 1 / (1 + np.exp(-x))


def plot_2d_clf_problem(X, y, h=None):
    '''
    Plots a two-dimensional labeled dataset (X,y) and, if function h(x) is given,
    the decision surfaces.
    '''
    assert X.shape[1] == 2, "Dataset is not two-dimensional"
    if h != None:
        # Create a mesh to plot in
        r = 0.02  # mesh resolution
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, r),
                             np.arange(y_min, y_max, r))
        XX = np.c_[xx.ravel(), yy.ravel()]
        try:
            Z_test = h(XX)
            if Z_test.shape == ():
                # h returns a scalar when applied to a matrix; map explicitly
                Z = np.array(list(map(h, XX)))
            else:
                Z = Z_test
        except ValueError:
            # can't apply to a matrix; map explicitly
            Z = np.array(list(map(h, XX)))
        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.contourf(xx, yy, Z, cmap=plt.cm.Pastel1)

    # Plot the dataset
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.tab20b, marker='o', s=50)


class Dataset:
    """

    """

    def __init__(self, file_path):
        """

        :param file_path:
        """
        data = read_data(file_path)

        self._X = data[0]
        self._y = data[1]
        self.path = file_path

    def __len__(self):
        """

        :return:
        """
        return len(self._X)

    def __getitem__(self, item):
        """

        :param item:
        :return:
        """
        return self._X[item], self._y[item]

    @property
    def X(self):
        """

        :return:
        """
        return self._X

    @property
    def y(self):
        """

        :return:
        """
        return self._y

    @property
    def data(self):
        """

        :return:
        """
        return self._X, self._y

    def __iter__(self):
        """

        :return:
        """
        return self.data

    def __str__(self):
        """

        :return:
        """
        return str(np.array([self._X, self._y]))
