"""
"""
import random
import numpy as np


def t_point_crossover(first, second, t=1):
    """

    :param first:
    :param second:
    :param t:
    :return:
    """
    if (t < 1) or (t > (len(first) - 1)):
        raise ValueError("Value of parameter k must be greater or equal to 1.")

    elif len(first) != len(second):
        raise ValueError("Chromosomes must be of equal length")

    new_data = np.array([])

    if t == 1:
        point = np.random.randint(0, len(first))
        new_data = np.concatenate((first.data[:point], second.data[point:]))

        return first.get_new_instance(new_data)

    points = sorted(int(np.round(x)) for x in random.sample(range(1,
                                                                  len(first)),
                                                            t))

    i = 0
    for k in points:
        if i == 0:
            new_data = np.concatenate((new_data, first.data[:k]))

        elif i % 2 == 0:
            new_data = np.concatenate((new_data, first.data[points[i - 1]: k]))

        elif i % 2 == 1:
            new_data = np.concatenate((new_data, second.data[points[i - 1]:
                                                             k]))

        i += 1

    if len(first.data) != len(new_data):
        if i % 2 == 0:
            new_data = np.concatenate((new_data, first.data[points[i - 1]:]))

        else:
            new_data = np.concatenate((new_data, second.data[points[i - 1]:]))

    return first.get_new_instance(new_data)


def uniform_crossover(first, second):
    """

    :param first:
    :param second:
    :return:
    """
    if len(first) != len(second):
        raise ValueError("Chromosomes must be of same length!")

    new_data = []

    for i, j in zip(first, second):
        p = random.uniform(0, 1)
        new_data.append(i if (p < 0.5) else j)

    return first.get_new_instance(new_data)


def cross1(first, second):
    """

    :param first:
    :param second:
    :return:
    """
    child = []
    for a, b in zip(first.data, second.data):
        child.append((a + b) / 2)

    return first.get_new_instance(np.array(child))


def cross2(first, second):
    """

    :param first:
    :param second:
    :return:
    """
    child = []

    for a, b in zip(first.data, second.data):
        if np.random.uniform(0, 1) < 0.5:
            child.append(a)
        else:
            child.append(b)

    return first.get_new_instance(np.array(child))


def cross3(first, second):
    """

    :return:
    """
    child = []
    pref = np.random.randint(len(first))

    for i, pair in enumerate(zip(first.data, second.data)):
        if i <= pref:
            child.append(pair[0])
        else:
            child.append(pair[1])

    return first.get_new_instance(np.array(child))
