"""
"""

import abc
import numpy as np
import random

import src.utils as utils


def generate_new_float_population(n, dim, error_function, lower=-1,
                                  upper=1, p=0.1):
    """

    :param n:
    :param dim:
    :param error_function:
    :param lower:
    :param upper:
    :return:
    """
    return [generate_new_float_chromosome(dim, error_function, lower,
                                          upper, p) for _ in
            range(n)]


def generate_new_float_chromosome(dim, error_function, lower=-1, upper=1,
                                  p=0.1):
    """

    :param dim:
    :param error_function:
    :param lower:
    :param upper:
    :return:
    """
    new_data = np.array([np.random.normal() * utils.SIGMA_2 for _ in range(
        dim)])
    return FloatChromosome(new_data, error_function, p=p)


class Chromosome(abc.ABC):
    """

    """

    def __init__(self, data):
        """

        :param data:
        """
        self._data = data
        self._error = None

    @property
    @abc.abstractmethod
    def error(self):
        """

        :return:
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def mutate(self):
        """
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_new_instance(self, data):
        """

        :param data:
        :return:
        """
        raise NotImplementedError()

    def eval(self):
        """

        :return:
        """
        return self.data

    @property
    def data(self):
        """

        :return:
        """
        return self._data

    def __eq__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() == other.error()

    def __gt__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() > other.error()

    def __lt__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() < other.error()

    def __ge__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() >= other.error()

    def __le__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() <= other.error()

    def __getitem__(self, item):
        """

        :param item:
        :return:
        """
        return self._data[item]

    def __len__(self):
        """

        :return:
        """
        return len(self.data)

    def __iter__(self):
        """

        :return:
        """
        return iter(self.data)


class FloatChromosome(Chromosome):
    """

    """

    def __init__(self, data, error_function, p=0.01):
        super().__init__(np.array(data))

        self._error_function = error_function
        self.p = p

    def error(self):
        """

        :return:
        """
        if self._error is None:
            self._error = self._error_function(self.data)

        return self._error

    def mutate(self, add=True, p=None, sigma=1):
        """

        """
        p = p if p else self.p

        if add:
            self._data = np.array(
                [x + np.random.normal() * sigma if (p >
                                                            random.uniform(
                                                                0,
                                                                1)) else x
                 for x in self.data])
        else:
            self._data = np.array([np.random.normal() * sigma if (
                    p > random.uniform(0, 1)) else x for x in self.data])

        return self

    def get_new_instance(self, data):
        """

        :param data:
        :return:
        """
        return FloatChromosome(data, self._error_function, self.p)
