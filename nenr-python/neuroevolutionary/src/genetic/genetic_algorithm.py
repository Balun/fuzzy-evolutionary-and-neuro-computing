"""

"""
import numpy as np

import src.genetic.selections as selections
import src.genetic.crossovers as crossovers
import src.utils as utils


class EliminationGeneticAlgorithm:
    """

    """

    def __init__(self, population, max_gen=5000, min_error=1e-7, k=3):
        """

        """
        self.population = sorted(population)
        self.max_gen = max_gen
        self.min_error = min_error
        self.k = k
        self.errors = []

    def run(self, trace=False):
        """

        :param trace:
        :return:
        """
        error = self.population[0].error()

        for i in range(self.max_gen):
            for j in range(len(self.population)):
                error = self.population[0].error()

                if error < self.min_error:
                    return self.population[0].data, error

                self.population = sorted(self.get_next_generation())

            error = self.population[0].error()
            self.errors.append(error)

            if trace:
                print("Generation %d: error -> %f" % (i, error))

        return self.population[0].data, error, np.array(self.errors)

    def get_next_generation(self):
        """

        :return:
        """
        tour = sorted(selections.tour_selection(self.population, self.k),
                      reverse=True)
        self.population.remove(tour[0])
        new_chromosome = self.crossover(tour[-1], tour[-2])
        new_chromosome = self.mutation(new_chromosome)
        self.population.append(new_chromosome)

        return self.population

    @property
    def optimal_params(self):
        """

        :return:
        """
        return self.population[0].data

    def crossover(self, first, second):
        """

        :param first:
        :param second:
        :return:
        """
        index = np.random.randint(3)

        if index == 0:
            return crossovers.cross1(first, second)

        elif index == 1:
            return crossovers.cross2(first, second)

        else:
            return crossovers.cross3(first, second)

    def mutation(self, chromosome):
        """

        :param chromosome:
        :return:
        """
        i = np.random.choice(np.arange(0, 3), p=[0.6, 0.3, 0.1])

        if i == 0:
            return chromosome.mutate(sigma=utils.SIGMA_1)

        elif i == 1:
            return chromosome.mutate(sigma=utils.SIGMA_2)

        else:
            return chromosome.mutate(sigma=utils.SIGMA_1, add=False)
