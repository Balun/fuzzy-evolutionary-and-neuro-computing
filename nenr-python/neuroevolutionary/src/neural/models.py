"""

"""

import numpy as np

import src.utils as utils


class NeuralNet:
    """

    :return:
    """

    def __init__(self, *layer_shapes):
        """

        :param layers:
        """
        if len(layer_shapes) < 2:
            raise ValueError("Must have at least 3 layers.")

        count = 0
        for layer in layer_shapes:
            count += layer

        self.layer_shapes = layer_shapes
        self.neurons = np.zeros(count)

    def get_params_size(self):
        """

        :return:
        """
        count = self.layer_shapes[0] * self.layer_shapes[1] * 2

        for i in range(2, len(self.layer_shapes)):
            count += self.layer_shapes[i] * (self.layer_shapes[i - 1] + 1)

        return count

    def set_input(self, x, y):
        """

        :param x:
        :param y:
        :return:
        """
        self.neurons[0] = x
        self.neurons[1] = y

    def eval(self, params):
        """

        :return:
        """
        start = 0
        end = 2
        param_id = 0

        for i in range(1, len(self.layer_shapes)):
            for j in range(self.layer_shapes[i]):
                net = 0

                for k in range(start, end):
                    x = self.neurons[k]

                    if i == 1:
                        net += np.abs(x - params[param_id]) / np.abs(
                            params[param_id + 1])
                        param_id += 2

                    else:
                        net += x * params[param_id]
                        param_id += 1

                self.neurons[end + j] = utils.f1(net) if i == 1 else \
                    utils.f2(net)

            start = end
            end += self.layer_shapes[i]

        return self.neurons

    def error(self, X, y, params):
        """

        :return:
        """
        error = 0
        N = len(self.neurons)

        for sample, label in zip(X, y):
            self.set_input(sample[0], sample[1])
            self.eval(params)

            error += (self.neurons[N - 3] - (1 if label == 0 else 0)) ** 2
            error += (self.neurons[N - 2] - (1 if label == 1 else 0)) ** 2
            error += (self.neurons[N - 1] - (1 if label == 2 else 0)) ** 2

        return error / len(X)

    def predict(self, X, params):
        """

        :return:
        """
        y = []

        for x in X:
            y.append(self.predict_single(x, params))

        return np.array(y)

    def predict_single(self, x, params):
        """

        :param sample:
        :return:
        """

        self.set_input(x[0], x[1])
        self.eval(params)

        return np.argmax(self.neurons[-3:])

    def display_net(self, params):
        """

        :return:
        """

        buff = ""
        start = 0
        param_id = 0

        for i in range(len(self.layer_shapes)):
            end = start + self.layer_shapes[i]

            buff += "Layer %d:\n" % i
            buff += "--------------------\n"

            buff += "Neurons: %s\n\n" % str(self.neurons[
                                            start:end])

            if i == 1:
                w = []
                s = []

                for j in range(start, end):
                    w.append(params[param_id])
                    s.append(params[param_id + 1])
                    param_id += 2

                buff += "w: %s\n\n" % str(w)
                buff += "s: %s\n\n" % str(s)

            elif i > 1:
                w = []

                for j in range(start, end):
                    w.append(params[param_id])
                    j += 1

                buff += "w: %s\n\n" % str(w)

            start = end
            buff += "\n"

        return buff
